'use strict';

const { TOKEN_TABLE } = require('../tables');

exports.up = function (knex, Promise) {

	return knex.schema.hasTable(TOKEN_TABLE).then(
		exists => {
			if (!exists) {
				return knex.schema.createTable(TOKEN_TABLE,
					table => {

						table.bigIncrements('id').primary();

						table.uuid('user');
						table.string('token', 128).notNullable();
						table.timestamp('expiresAt', 'UTC').notNullable();

						table.timestamp('createdAt', 'UTC').defaultTo(knex.fn.now());
					});
			}
		});
};

exports.down = function (knex, Promise) {
	return knex.schema.dropTableIfExists(TOKEN_TABLE);
};