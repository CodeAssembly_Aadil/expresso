'use strict';

const { OTP_TABLE } = require('../tables');

exports.up = function (knex, Promise) {

	return knex.schema.hasTable(OTP_TABLE).then(
		exists => {
			if (!exists) {
				return knex.schema.createTable(OTP_TABLE,
					table => {

						table.bigIncrements('OTP').primary();

						table.uuid('userID');
						table.string('otp', 16).notNullable();
						table.timestamp('expiresAt', 'UTC').notNullable();

						table.timestamp('createdAt', 'UTC').defaultTo(knex.fn.now());
					});
			}
		});
};

exports.down = function (knex, Promise) {
	return knex.schema.dropTableIfExists(OTP_TABLE);
};