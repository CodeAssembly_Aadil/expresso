'use strict';

const { SET_UPDATED_AT_TIMESTAMP } = require('../fucns');

exports.up = function (knex, Promise) {
	return knex.schema.raw(`
        CREATE OR REPLACE FUNCTION ${SET_UPDATED_AT_TIMESTAMP}()
            RETURNS TRIGGER AS $$ 
        BEGIN
            -- Check for changes since last update
            IF row(NEW.*) IS DISTINCT FROM row(OLD.*) THEN 
                NEW.updatedAt = now(); 
                -- Update and return new record
                RETURN NEW;
            ELSE
                -- Ignore and return existing record
                RETURN OLD;
            END IF;
        END;
        $$ LANGUAGE plpgsql IMMUTABLE STRICT;
`);
};

exports.down = function (knex, Promise) {
	return knex.schema.raw(`DROP FUNCTION IF EXISTS ${SET_UPDATED_AT_TIMESTAMP}();`);
};
