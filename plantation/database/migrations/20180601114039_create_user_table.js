'use strict';

const { USER_TABLE } = require('../tables');

exports.up = function (knex, Promise) {

	return knex.schema.hasTable(USER_TABLE).then(
		exists => {
			if (!exists) {
				return knex.schema.createTable(USER_TABLE,
					table => {

						table.uuid('id').defaultTo(knex.raw('uuid_generate_v4()')).primary();

						table.string('firstName', 255);
						table.string('lastName', 255);
						table.string('phoneNumber', 64).unique();

						table.timestamp('acceptedTermsAt', 'UTC').nullable();
						table.timestamp('lockedAt', 'UTC').nullable();
						table.timestamp('activatedAt', 'UTC').nullable();

						table.timestamp('createdAt', 'UTC').defaultTo(knex.fn.now());
						table.timestamp('updatedAt', 'UTC').defaultTo(knex.fn.now());
					});
			}
		});
};

exports.down = function (knex, Promise) {
	return knex.schema.dropTableIfExists(USER_TABLE);
};
