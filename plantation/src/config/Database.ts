import * as DotEnv from 'dotenv';

DotEnv.config({ encoding: 'utf8', path: '.env' });

export const Database = {
	client: 'pg',
	connection: {
		database: process.env.POSTGRES_DB,
		host: process.env.POSTGRES_HOST,
		password: process.env.POSTGRES_PASSWORD,
		port: process.env.POSTGRES_PORT,
		user: process.env.POSTGRES_USER,
	},
	pool: {
		max: 10,
		min: 2,
	},
	migrations: {
		directory: './database/migrations',
		extension: '.js',
		loadExtensions: ['.js'],
		tableName: 'knex_migrations',
	},
	seeds: {
		directory: './database/seeds',
		loadExtensions: ['.js'],
	},
};
