export const USER_TABLE = 'User';
export const TOKEN_TABLE = 'Token';
export const OTP_TABLE = 'OTP';
export const SMS_TABLE = 'SMS';
export const SMS_STATUS_TABLE = 'SMSStatus';
export const SMS_USER_TABLE = 'SMSUser';
