import * as DotEnv from 'dotenv';
import * as Glue from 'glue';
import { Server } from 'hapi';

DotEnv.config({ encoding: 'utf8', path: '.env' });

const PROD_MODE: boolean = process.env.NODE_ENV === 'production';

const manifest: Glue.Manifest = {
	server: {
		host: process.env.HOST,
		port: process.env.PORT,
		debug: PROD_MODE ? false : { request: ['*'] },
		compression: false,
	},

	register: {
		plugins: [
			'app/authentication/AuthenticationPlugin',
			'app/users/UserPlugin',
		],
	},
};

const startServer = async () => {
	try {
		const server: Server = await Glue.compose(manifest);
		await server.start();
		console.info(`${new Date()} Server running at: ${server.info.uri}`);
	} catch (error) {
		console.log(error);
		process.exit(1);
	}
};

startServer();
