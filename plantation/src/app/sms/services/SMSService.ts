import Axios from 'axios';
import { DateTime } from 'luxon';
import uuid from 'uuid/v4';

import { isString } from 'lodash';

import { SMS } from 'app/sms/models/SMS';
import SMSRepository from 'app/sms/repository/SMSRepository';
import User from 'app/users/models/User';
import UserRepository from 'app/users/repositories/UserRepository';

// Implementaion

export default class SMSService {

	static readonly CLICKATELL_API_SEND: string = 'https://platform.clickatell.com/messages';

	static async sendSMS(to: string | string[], content: string, validityPeriod: number = 1440, scheduledDeliveryTime: Date = null) {

		const phoneNumbers: string[] = isString(to) ? [to] : to;

		const users: User[] = await UserRepository.findUsersByPhoneNumber(phoneNumbers, ['phoneNumber', 'id']);

		const sms: SMS = {
			clientMessageId: uuid(),
			to: (to instanceof String) ? [to] : to,
			content,
			binary: false,
			from: process.env.TWO_WAY_NUMBER,
			validityPeriod,
			scheduledDeliveryTime,
			charset: 'UTF-8',
		};

		SMSRepository.saveSMS(sms);

		const config = {
			headers: {
				'Content-Type': 'application/json',
				'Accept': 'application/json',
				'Authorization': process.env.CLICKATELL_API_KEY,
			},
		};

		/** @link https://www.clickatell.com/developers/api-documentation/rest-api-send-message/ */
		const response = await Axios.post(SMSService.CLICKATELL_API_SEND, params, config);

		const message = response.data.messages[0];

		if (message.error) {
			throw new Error(message.error);
		} else if (response.data.error) {
			throw new Error(response.data.error);
		}

		return message.accepted;
	}

}

module.exports = SMSService;
