export const enum SMSError {

}
'Invalid or missing integration API Key' 	The integration API key is either incorrect or has not been included in the API call.
'Integration is not active' 	Your integration is not active. Try to publish your integration first.
'Account is not active' 	The account is not active.
'Empty message content' 	Message content is empty.
'Empty receivers list' 	Destination list is empty.
'Message receivers list is too long' 	Too many destination addresses.
'Two-way integration error – You should specify correct from number' 	Sender number was not specified or is not connected to your integration.
'Two-way integration error – You should use two-way integration to specify from number' 	User specified from number but integration isn't two-way.
'Unsupported charset. - [charset] is not supported' 	The specified character set is not supported.
'ScheduledDeliveryTime format is incorrect' 	ScheduledDeliveryTime format must be yyyy-MM-dd'T'HH:mm:ssZ
IP lock down violation. – Requests from [ip] forbidden according to settings 	Couldn't determine user's IP address or it is not in whitelist.
'Maximum message parts exceeded.' – PartCount is: [partCount] 	The text component of the message is greater than the permitted 160 characters (70 Unicode characters).
Message exceeds max available characters: [defaultMaxMessageLength] – Please enable message parts to send the message 	The text component of the message is greater than the permitted 160 characters (70 Unicode characters).
Duplicated destination address found: [] 	The destination number you are attempting to send to is duplicated.
'Number opted out'	The user has opted out and is no longer subscribed to your service.
'In order to send to US numbers, you should use Two-way integration' 	In order to send to US numbers, you need to create a two-way integration.
'In order to send to US numbers, you should enable STOP/SUBSCRIBE commands in the integration' 	In order to send to US numbers, you should enable STOP/SUBSCRIBE commands in the integration
'Destination country doesn't support two-way messaging' 	The specified destination country doesn't support two-way messaging.
'Invalid destination address' 	The destination number you are attempting to send to is invalid
'Daily quota exceeded' 	Certain daily and monthly limits are in place to safeguard against abuse of the system. This limit only applies to your test phones - you will still be able to send test messages and receive simulated responses from the system.
'Monthly quota exceeded' 	Certain daily and monthly limits are in place to safeguard against abuse of the system. This limit only applies to your test phones - you will still be able to send test messages and receive simulated responses from the system.
'Not Enough Money' – You don't have enough money to complete this operation 	Insufficient balance to complete the operation.