/**
 * Clickatell SMS Message format
 *
 * https://www.clickatell.com/developers/api-documentation/rest-api-status-callback/
 */
export interface SMSStatus {
	/** Name of integration used for delivery */
	integrationName: string;
	/** ID of the message */
	messageId: string;
	/** ID of request used for delivery */
	requestId: string;
	/** Unique client message identifier (UUID) */
	clientMessageId: string;
	/** Recipient number */
	to?: string;
	/** Purchased two-way number */
	from?: string;
	/** Numerical reference for status of message */
	statusCode: number;
	/** Status of the message */
	status: string;
	/** Description of the status */
	statusDescription: string;
	/** Timestamp of last message's status */
	timestamp: Date;
}
