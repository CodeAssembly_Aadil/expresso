import { Plugin, PluginBase, PluginNameVersion, Request, ResponseToolkit, Server, ServerAuthScheme } from 'hapi';

import AuthBearerTokenStrategy from 'app/authentication/strategies/AuthBearerTokenStrategy';
import OTPStrategy from 'app/authentication/strategies/OTPStrategy';

const AuthenticationPlugin: Plugin<PluginNameVersion> = {

	name: 'Plantation.Authentication',
	version: '1.0.0',
	multiple: false,
	dependencies: undefined,
	once: true,

	async register(server: Server, options: any): Promise<void> {

		AuthBearerTokenStrategy('auth-bearer-token-strategy', server);
		OTPStrategy('otp-strategy', server);

		server.route({
			method: 'GET',
			path: '/test',
			handler(request: Request, h: ResponseToolkit) {
				return 'hello, world';
			},
		});

		server.route({
			method: 'GET',
			path: '/',
			handler(request: Request, h: ResponseToolkit) {
				return 'Plantation';
			},
		});
	},
};

module.exports = AuthenticationPlugin;
