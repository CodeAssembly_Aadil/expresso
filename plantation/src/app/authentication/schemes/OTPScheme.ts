import * as Boom from 'boom';

import { isPlainObject } from 'lodash';

import {
	AuthenticationData,
	Lifecycle,
	Request,
	ResponseToolkit,
	Server,
	ServerAuthScheme,
	ServerAuthSchemeObject,
} from 'hapi';

// Internals

const OTP = 'OTP';

// Implementation

export default function OTPScheme(server: Server, options?: any): ServerAuthSchemeObject {
	return {
		async authenticate(request: Request, h: ResponseToolkit): Promise<Lifecycle.ReturnValue | void> {

			const { otp, phoneNumber } = request.payload as any;

			if (!otp || !phoneNumber) {
				throw Boom.unauthorized('Missing authorization', OTP);
			}

			const { isValid, credentials, response } = await options.validate(request, phoneNumber, otp, h);

			if (response !== undefined) {
				return h.response(response).takeover();
			}

			const data: AuthenticationData = { credentials };

			if (!isValid) {
				return h.unauthenticated(Boom.unauthorized('Bad OTP', OTP), data);
			}

			if (!credentials || !isPlainObject(credentials)) {
				throw Boom.badImplementation('Bad credentials');
			}

			return h.authenticated(data);
		},

	};
}
