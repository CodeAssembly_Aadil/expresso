import DB from 'app/core/database/DB';

import OTP from 'app/authentication/models/OTP';
import Token from 'app/authentication/models/Token';

import { OTP_TABLE, TOKEN_TABLE } from 'app/core/database/Tables';

export default class AuthRepository {

	static async saveTokenForUser(userID: string, token: string, expiresAt: Date): Promise<Token> {
		const tokenData: Token = {
			user: userID,
			token,
			expiresAt,
		};

		return await DB(TOKEN_TABLE)
			.insert(tokenData, ['*']);
	}

	static async saveOTPForUser(userID: string, otp: string, expiresAt: Date): Promise<OTP> {
		const otpData: OTP = {
			user: userID,
			otp,
			expiresAt,
		};

		return await DB(OTP_TABLE)
			.insert(otpData, ['*']);
	}

	static async findOTPForUserID(userID: string): Promise<OTP> {
		const otpData: OTP = {
			user: userID,
			otp,
			expiresAt,
		};

		return await DB(OTP_TABLE)
			.insert(otpData, ['*']);
	}
}
