export default interface OTP {
	id?: number;

	user: string;
	otp: string;
	expiresAt: Date;

	createdAt?: Date;
}
