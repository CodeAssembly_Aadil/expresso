export default interface Token {
	id?: number;

	user: string;
	token: string;
	expiresAt: Date;

	createdAt?: Date;
}
