import { Request, ResponseToolkit } from 'hapi';

import * as Boom from 'boom';

import { DateTime } from 'luxon';

import OTP from 'app/authentication/models/OTP';
import AuthRepository from 'app/authentication/repository/AuthRepository';
import OTPService from 'app/authentication/services/OTPService';
import User from 'app/users/models/User';
import UserRepository from 'app/users/repositories/UserRepository';
import SMSService from 'app/sms/services/SMSService';

export default class AuthenticationService {

	static async generateOTP(request: Request, h: ResponseToolkit) {

		const { phoneNumber } = request.payload as any;

		let user: User;
		let otp: OTP;

		try {

			user = await UserRepository.findUserByPhone(phoneNumber);

			if (!user) {
				// log error possibly fishing - fail silently;
				// return Boom.
			}

			const otpLifetime: number = Number.parseInt(process.env.OTP_LIFETIME);
			const otpLifetimeMinutes: number = Math.floor(otpLifetime / 60);

			const oneTimePin: string = await OTPService.generateOTP();
			const expiresAt: DateTime = DateTime.utc().plus({ minutes: otpLifetimeMinutes });

			otp = await AuthRepository.saveOTPForUser(user.uuid, oneTimePin, expiresAt.toJSDate());

			const friendlyDate: string = expiresAt.toLocaleString(DateTime.TIME_WITH_SHORT_OFFSET);

			const otpMessage = `Expresso One Time Pin. This code is valid for ${otpLifetimeMinutes} minutes. OTP: ${oneTimePin}`;

			SMSService.sendSMS(user.phoneNumber, otpMessage, otpLifetimeMinutes);

		} catch (error) {

			return Boom.badImplementation('UserController.store', error);
		}

		return user;
	}
}
