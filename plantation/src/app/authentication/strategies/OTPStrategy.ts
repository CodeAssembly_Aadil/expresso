import { Request, ResponseToolkit, Server } from 'hapi';

import { pick } from 'lodash';

import OTPScheme from 'app/authentication/schemes/OTPScheme';
import User from 'app/users/models/User';
import UserRepository from 'app/users/repositories/UserRepository';

const OTP: string = 'OTP';

async function validateOTP(request: Request, phoneNumber: string, otp: string, h: ResponseToolkit) {

	const user: User = await UserRepository.findUserByPhoneNumberWithOTP(phoneNumber, otp);

	let isValid = false;
	let credentials = null;

	if (Boolean(user)) {
		// User exists in DB user is activated and user is not locked out
		isValid = Boolean(user.activatedAt) && !Boolean(user.lockedAt);
		credentials = pick(user, ['uuid', 'firstName', 'lastName', 'phoneNumber', 'createdAt', 'updatedAt']);
	}

	return { isValid, credentials };
}

export default function OTPStrategy(schemeID: string, server: Server) {
	server.auth.scheme(schemeID, OTPScheme);
	server.auth.strategy(OTP, schemeID, {
		validate: validateOTP,
	});
}
