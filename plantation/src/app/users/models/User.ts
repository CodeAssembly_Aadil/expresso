export default interface User {
	uuid?: string;

	firstName: string;
	lastName: string;
	phoneNumber: string;

	acceptedTermsAt?: Date;
	lockedAt?: Date;
	activatedAt?: Date;

	createdAt?: Date;
	updatedAt?: Date;
}
