import * as Joi from 'joi';

import { PhoneNumberSchema } from 'app/core/schemas/PhoneNumber';

export const RegisterUserSchema: Joi.Schema = Joi.object().keys(
	{
		firstName: Joi.string()
			.regex(/^[a-zA-Z0-9_ -]{1,255}$/)
			.required()
			.label('First Name'),

		lastName: Joi.string()
			.regex(/^[a-zA-Z0-9_ -]{1,255}$/)
			.required()
			.label('Last Name'),

		phoneNumber: PhoneNumberSchema,

		acceptedTerms: Joi.boolean()
			.invalid(false)
			.required()
			.label('Terms and Conditions'),
	});
