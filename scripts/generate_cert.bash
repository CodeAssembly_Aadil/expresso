#!/bin/bash

export NAME="plantation.local"
export KEYSIZE=3072

# Certificate details; replace items in angle brackets with your own info
export SUBJECT="
/
/C=ZA\
/ST=Gauteng\
/L=Centurion\
/O=Code Assembly Pty Ltd\
/CN=*.plantation.local\
/emailAddress=aadil@codeassembly.co.za\
"

# Generate a Private Key
export PASSPHRASE=$(head -c 512 /dev/urandom | tr -dc a-z0-9A-Z | head -c 128; echo)

# Generate the server private key
openssl genrsa -aes256 -out ../ssl/$(echo -n $NAME).pem -passout env:PASSPHRASE $(echo -n $KEYSIZE)

# Generate a CSR (Certificate Signing Request)
openssl req -new -batch -subj "$(echo -n $SUBJECT)" -key ../ssl/$(echo -n $NAME).pem -out ../ssl/$(echo -n $NAME).csr -passin env:PASSPHRASE

# Remove Passphrase from Key
cp ../ssl/$(echo -n $NAME).pem ../ssl/$(echo -n $NAME).key
openssl rsa -in ../ssl/$(echo -n $NAME).key -out ../ssl/$(echo -n $NAME).key -passin env:PASSPHRASE

# Generating a Self-Signed Certificate (365 days)
openssl x509 -req -days 365 -in ../ssl/$(echo -n $NAME).csr -signkey ../ssl/$(echo -n $NAME).key -out ../ssl/$(echo -n $NAME).crt

# Generating DH parameters
openssl dhparam $(echo -n $KEYSIZE) -out ../ssl/$(echo -n $NAME).dhparam.pem
