import React from 'react';

const Button = (props: any) => (
	<button>
		{props.children}
	</button>
);

export default Button;